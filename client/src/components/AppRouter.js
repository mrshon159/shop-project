import React, {useContext, useEffect} from 'react';
import {Routes, Route, Navigate} from 'react-router-dom'
import {adminAuthRoutes, authRoutes, publicRoutes} from "../routes";
import DevicePage from "../pages/DevicePage";
import Shop from "../pages/Shop";
import {SHOP_ROUTE} from "../utils/consts";
import {Context} from "../index";
import {check} from "../http/userAPI";

const AppRouter = () => {
    const {user} = useContext(Context)


    useEffect(() => {
    }, [user.setUser])

    const authRole = (auth, role) => {
        if (auth) {
            if (role === 'ADMIN') {
                return adminAuthRoutes.map(({path, component}) =>
                    <Route key={path} path={path} element={component} exact/>
                )
            } else if (role === 'USER') {
                return authRoutes.map(({path, component}) =>
                    <Route key={path} path={path} element={component} exact/>
                )
            }
        } else {
            return publicRoutes.map(({path, component}) =>
                    <Route key={path} path={path} element={component}/>
                )
        }
    }




    return (
        <Routes>
            {
                authRole(user.isAuth, user.user.role)
            }
            <Route
                path="*"
                element={<Navigate to={SHOP_ROUTE} replace />} />
        </Routes>
    );
};

export default AppRouter;