import React, {useContext, useEffect} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {ListGroup} from 'react-bootstrap'
import {fetchDevice} from "../http/deviceAPI";

const TypeBar = observer(() => {
    const {device} = useContext(Context)

    const allDeviceView = () => {
        device.setSelectedType({})
        device.setSelectedBrand({})

        fetchDevice(null, null, device.page, 4).then(data => {
            device.setDevices(data.rows)
            device.setTotalCount(data.count)
        })
    }
    return (
        <ListGroup>
            <ListGroup.Item
                className="p-2"
                style={{cursor: 'pointer'}}
                active={device.selectedType.id  === undefined}
                onClick={() =>  allDeviceView()}
            >
                Показать все
            </ListGroup.Item>
            {device.types.map(type =>
                <ListGroup.Item
                    className="p-2"
                    style={{cursor: 'pointer'}}
                    active={type.id === device.selectedType.id}
                    onClick={() => device.setSelectedType(type)}
                    key={type.id}
                >
                    {type.name}
                </ListGroup.Item>
            )}
        </ListGroup>
    );
});

export default TypeBar;