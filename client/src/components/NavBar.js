import React, {useContext} from 'react';
import {Button, Container, Nav, Navbar} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import {ADMIN_ROUTE, LOGIN_ROUTE, SHOP_ROUTE} from "../utils/consts";
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import {useNavigate} from "react-router-dom"

const NavBar = observer(() => {
    const navigate = useNavigate()
    const {user} = useContext(Context)

    const logout = () => {
        user.setUser = ({})
        user.setIsAuth(false)
        localStorage.setItem('token', "")
        navigate(LOGIN_ROUTE)
    }

    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <NavLink to={SHOP_ROUTE} style={{color: "white", textDecoration: "none"}}>DEVICE</NavLink>
                {user.isAuth ?
                    <Nav className="w-100 justify-content-end" style={{color: "white"}}>
                        {user.user.role === 'ADMIN'
                        &&
                        <Button
                            variant={"outline-light"}
                            onClick={() => navigate(ADMIN_ROUTE)}
                        >
                            Админ панель
                        </Button>}
                        <Button
                            variant={"outline-light"}
                            className="ms-2"
                            onClick={() => logout()}
                        >
                            Выйти
                        </Button>
                    </Nav>
                    :
                    <Nav className="w-100 justify-content-end" style={{color: "white"}}>
                        <Button variant={"outline-light"} onClick={() => navigate(LOGIN_ROUTE)}>Авторизация</Button>
                    </Nav>
                }
            </Container>
        </Navbar>
    );
})

export default NavBar;