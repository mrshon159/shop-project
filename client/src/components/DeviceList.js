import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Row} from "react-bootstrap";
import DeviceItem from "./DeviceItem";

const DeviceList = observer(() => {
    const {device} = useContext(Context)
    const deviceBrands = []

    device.brands.map(brand => {
        deviceBrands.push({id: brand.id, name: brand.name})
    })


    return (
        <Row>
            {device.devices.map(device =>
                <DeviceItem key={device.id} device={device} brand={deviceBrands.find(brand => device.brandId === brand.id)}/>
            )}
        </Row>
    );
});

export default DeviceList;