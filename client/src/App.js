import React, {useContext, useEffect, useState} from "react";
import {BrowserRouter as Router} from "react-router-dom";
import AppRouter from "./components/AppRouter";
import NavBar from "./components/NavBar";
import {observer} from "mobx-react-lite";
import {Context} from "./index";
import {check} from "./http/userAPI";
import {Spinner} from "react-bootstrap";

const App = observer(() => {
    const {user} = useContext(Context)
    const [loading, setLoading] = useState(true)



    useEffect(() => {
        if (localStorage.token !== "") {
                check().then(data => {
                    user.setIsUser(data)
                    user.setIsAuth(true)
                }).finally(() => setLoading(false))
        } else {
            setLoading(false)
        }
    }, [])

    if (loading) {
        return (
            <div style={{height: "100vh"}} className="d-flex align-items-center justify-content-center">
                <Spinner animation="grow" />
            </div>
        )
    }

    return (
        <div className="App">
            <Router>
                <NavBar />
                <AppRouter />
            </Router>
        </div>
    );
});

export default App;
