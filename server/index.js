require('dotenv').config()
const express = require('express')
const sequelize = require('./db')
const models = require('./models/models')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const router = require('./routes/index')
const errorHandler = require('./middleware/ErrorHandlingMiddleware')
const path = require('path')
const PORT = process.env.PORT

const app = express()
//Cors - для запросов к БД
app.use(cors())
//express.json для того, чтобы парсить по JSON
app.use(express.json())
app.use(express.static(path.resolve(__dirname, 'static')))
app.use(fileUpload({}))
app.use('/api', router)

//Обработка ошибок, всегда стоит последним Middleware
app.use(errorHandler)

//Функций для подключения к БД
const start = async () => {
    try {
        //С помощью этой фунции идет подключение к БД
        await sequelize.authenticate()
        //Эта функция сверяет БД со схемой данных
        await sequelize.sync()
        app.listen(PORT, () => console.log(`start server on port ${PORT}`))
    } catch (e) {
        console.log(e)
    }
}

start()