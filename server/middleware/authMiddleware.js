const jwt = require('jsonwebtoken')

module.exports = function (req, res, next) {
    //Если метод не PUT, GET, DELETE, пропускаем
    if (req.method === 'OPTIONS') {
        next()
    }
    //Проверка на ошибку
    try {
        //Берем токен из хедера, в данном случаи в хедере храниться "Bearer adadsdadawdawdad12331", берем по первому элементу
        const token = req.headers.authorization.split(' ')[1]
        if (!token) {
            res.status(401).json({message: 'Пользователь не авторизован'})
        }
        //Декодируем токен, первым аргументом идет токен, вторым - секретный ключ, который храниться в ENV
        const decoded = jwt.verify(token, process.env.SECRET_KEY)
        req.user = decoded
        next()
    } catch (e) {
        res.status(401).json({message: 'Пользователь не авторизован'})
    }
}