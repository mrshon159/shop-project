class ApiError extends Error{
    constructor(status, message) {
        super();
        this.status = status
        this.message = message
    }

    static badRequest(message) {
        return new ApiError(404, message)
    }
    //Сервер не может корректно обработать запрос
    static internal(message) {
        return new ApiError(500, message)
    }
    //Отсутствие доступа материала, которое пытаетесь загрузить
    static forbidden(message) {
        return new ApiError(403, message)
    }
}

module.exports = ApiError