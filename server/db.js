const {Sequelize} = require('sequelize')

//Подключение базы данных
module.exports = new Sequelize(
    process.env.DB_NAME, // Название БД
    process.env.DB_USER, // Пользователь БД
    process.env.DB_PASSWORD, // Пароль пользователя БД
    {
        dialect: 'postgres',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT
    }
)