const uuid  = require('uuid')
const path = require('path')
const {Device, DeviceInfo} = require('../models/models')
const ApiError = require('../error/ApiError')

class DeviceController {
    async create (req, res, next) {
        try {
            let {name, price, brandId, typeId, info} = req.body
            //Для того, чтобы загрузить файлы,следует установить express-fileupload
            const {img} = req.files
            //uuid - уникальное имя для файла
            let fileName = uuid.v4() + ".jpg"
            //mv - путь до файла
            img.mv(path.resolve(__dirname, '..', 'static', fileName))
            const device = await Device.create({name, price, brandId, typeId, img: fileName})

            if (info) {
                //Данные через форм-дату приходят в виде строки, поэтому массив нужно парсить
                //На фронте в JSON - строку, а на бэк, в JS-объекты => с помощью ф-ции parse
                info = JSON.parse(info)
                info.forEach(i =>
                    DeviceInfo.create({
                        title: i.title,
                        description: i.description,
                        deviceId: device.id
                    })
                )
            }
            return res.json(device)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }
    async getAll (req, res) {
        let {brandId, typeId, limit, page} = req.query
        page = page || 1
        limit = limit || 9
        let offset = page * limit - limit
        let devices
        //Сразу сортировка по бренду или типу, или бренду и типу, если они указаны, как опции, иначе возвращаеются все строки БД
        if (!brandId && !typeId) {
            devices = await Device.findAndCountAll({limit, offset})
        }
        if (!brandId && typeId) {
            devices = await Device.findAndCountAll({where: {typeId}, limit, offset})
        }
        if (brandId && !typeId) {
            devices = await Device.findAndCountAll({where: {brandId}, limit, offset})
        }
        if (brandId && typeId) {
            devices = await Device.findAndCountAll({where: {typeId, brandId}, limit, offset})
        }

        return res.json(devices)
    }
    async getOne (req, res) {
        const {id} = req.params
        const device = await Device.findOne({
            where: {id},
            //Добавляем поля информации об устройстве. Т.к при открытии устройства, ожидается увидеть о нем информацию
            include: [{model: DeviceInfo, as: 'info'}]
        })
        return res.json(device)
    }
}

module.exports = new DeviceController()