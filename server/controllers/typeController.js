const {Type} = require('../models/models')
const ApiError = require('../error/ApiError')

class TypeController {
    //Добавление в базу данных
    async create (req, res) {
        const {name} = req.body
        const type = await Type.create({name})
        return res.json(type)
    }
    async getAll (req, res) {
        //findAll - получить все объекты из БД
        const types = await Type.findAll()
        return res.json(types)
    }
}

module.exports = new TypeController()