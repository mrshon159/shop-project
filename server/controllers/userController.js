const {User, Basket} = require('../models/models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const ApiError = require('../error/ApiError')

const generateJwt = (id, email, role) => {
    return jwt.sign({id, email, role}, process.env.SECRET_KEY,
        {
            expiresIn: '24h'
        })
}

class UserController {
    async registration (req, res, next) {
        //Берем маил, пароль и роль(админ, пользователь)
        const {email, password, role} = req.body
        //Проверка введен ли маил или пароль
        if(!email || !password) {
            return next(ApiError.badRequest('Не введен маил или пароль'))
        }
        const candidate = await User.findOne({where: {email}})
        //Проверка существует ли введеный пользователь
        if(candidate) {
            return next(ApiError.badRequest('Такой пользователь уже существует'))
        }
        //Хэшируем пароль с помощью bcrypt (npm i bcrypt). Первым аргументом принимает пароль, вторым, сколько раз захэшировать
        const hashPassword = await bcrypt.hash(password, 5)
        //Создаем пользователя и передаем маил, его роль и захэшированный пароль
        const user = await User.create({email, role, password: hashPassword})
        //Создаем корзину для определенного пользователя
        const basket = await Basket.create({userId: user.id})
        //Создаем токен, он принимает в себя payload(объект, в который помещаем ид пользователя, маил и его роль),
        // а вторым аргументом секретный ключ, третий аргумент - опции, в данном случаи, сколько живет токен (24 часа), после уничтожается
        const token = generateJwt(user.id, user.email, user.role)

        return res.json({token})

    }
    async login (req, res, next) {
        const {email, password} = req.body
        //Пробуем найти пользователя в БД
        const user = await User.findOne({where: {email}})
        //Если не найден, выдаем ошибку
        if (!user) {
            return next(ApiError.internal('Пользователь с таким именем не найден'))
        }
        //Проверка пароля через compareSync, функция синхронная
        let comparePassword = bcrypt.compareSync(password, user.password)
        if (!comparePassword) {
            return next(ApiError.internal('Указа не верный пароль'))
        }
        const token = generateJwt(user.id, user.email, user.role)

        return res.json({token})

    }
    async check (req, res, next) {
        //Если пользователь постоянно использует свой акк, токен будет перезаписываться
        const token = generateJwt(req.user.id, req.user.email, req.user.role)
        return res.json({token})
    }
}

module.exports = new UserController()